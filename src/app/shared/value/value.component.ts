import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-value',
  templateUrl: './value.component.html',
  styleUrls: ['./value.component.scss'],
})
export class ValueComponent {
  @Input() value: any;
  @Input() title: string;
  @Output() didActivate = new EventEmitter<void>();
}
