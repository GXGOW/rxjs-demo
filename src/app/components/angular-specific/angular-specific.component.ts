import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-angular-specific',
  templateUrl: './angular-specific.component.html',
  styleUrls: ['./angular-specific.component.scss'],
})
export class AngularSpecificComponent implements OnInit {
  formGroup: FormGroup = this.formBuilder.group({
    firstName: [],
    lastName: [],
    email: [undefined, [Validators.required, Validators.email]],
  });

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit(): void {}
}
