import { Component } from '@angular/core';
import {
  Observable,
  Subject,
  BehaviorSubject,
  ReplaySubject,
  AsyncSubject,
} from 'rxjs';

@Component({
  selector: 'app-creation',
  templateUrl: './creation.component.html',
  styleUrls: ['./creation.component.scss'],
})
export class CreationComponent {
  observableValue;
  subjectValue;

  subject = new AsyncSubject();

  triggerObservable() {
    new Observable((subscriber) => {
      subscriber.next(123);
      // subscriber.error('oeps');
      subscriber.complete();
    }).subscribe((value) => {
      this.observableValue = value;
    });
  }

  triggerSubject() {
    this.subject.subscribe((value) => {
      this.subjectValue = value;
    });

    this.subject.next(123);
    this.subject.complete();
    // this.subject.next(123);
    // this.subject.next(1);
    // this.subject.next(1);
  }
}
