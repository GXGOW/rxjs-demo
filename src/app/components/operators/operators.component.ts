import { Component, OnDestroy } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import {
  combineLatest,
  forkJoin,
  from,
  fromEvent,
  interval,
  of,
  Subject,
  Subscription,
  throwError,
  timer,
} from 'rxjs';
import {
  catchError,
  delay,
  distinctUntilChanged,
  filter,
  finalize,
  first,
  retry,
  switchMap,
  take,
  takeUntil,
  tap,
} from 'rxjs/operators';

@UntilDestroy()
@Component({
  selector: 'app-operators',
  templateUrl: './operators.component.html',
  styleUrls: ['./operators.component.scss'],
})
export class OperatorsComponent {
  creationValue;
  filterValue;
  combinationValue;
  errorHandlingValue;
  utilityValue;

  subject = new Subject();

  constructor(private _snackbar: MatSnackBar) {}

  // of, from, fromEvent, interval, timer
  triggerCreation() {
    interval(1000)
      .pipe(tap(console.log), untilDestroyed(this))
      .subscribe((value) => {
        this.creationValue = value;
      });
  }

  // filter, first, take, distinctUntilChanged
  triggerFiltering() {
    interval(1000)
      .pipe(
        filter((value) => value > 2),
        take(2),
        distinctUntilChanged()
      )
      .subscribe((value) => {
        this.filterValue = value;
      });
  }

  // combineLatest, forkJoin
  triggerCombination() {
    combineLatest([of('observable 1'), of('obsverable 2')]).subscribe(
      (value) => {
        this.combinationValue = value.toString();
      }
    );
  }

  // catchError, retry, finalize
  triggerErrorHandling() {
    of('test')
      .pipe(
        // switchMap(() => throwError('error')),
        finalize(() => 'finally')
      )
      .subscribe((value) => {
        this.errorHandlingValue = value;
      });
  }

  // tap, delay, timeInterval, repeat, toPromise
  triggerUtility() {
    from([1, 3, 5, 7]).pipe(
      tap((value) => {
        console.log(value);
      })
    );
  }
}
