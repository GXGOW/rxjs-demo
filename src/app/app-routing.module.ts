import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AngularSpecificComponent } from './components/angular-specific/angular-specific.component';
import { AngularSpecificGuard } from './components/angular-specific/angular-specific.guard';
import { CreationComponent } from './components/creation/creation.component';
import { OperatorsComponent } from './components/operators/operators.component';

const routes: Routes = [
  {
    path: 'creation',
    component: CreationComponent,
  },
  {
    path: 'operators',
    component: OperatorsComponent,
  },
  {
    path: 'angular-features',
    component: AngularSpecificComponent,
    canActivate: [AngularSpecificGuard],
  },
  {
    path: '**',
    redirectTo: 'creation',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
